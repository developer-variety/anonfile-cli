﻿using System;
using System.IO;
using System.Linq;
using AnonFileAPI;

namespace AnonFileCLI
{
    internal static class Program
    {
        public static void Main(string[] args)
        {
            if (!args.Any()) return;
            foreach (string arg in args)
            {
                Console.WriteLine($"Uploading {arg}.");
                
                using (AnonFileWrapper anonFileWrapper = new AnonFileWrapper())
                {
                    AnonFile anonFile = anonFileWrapper.UploadFile(Path.GetFullPath(arg));
                    Console.WriteLine("Error Code: {0}", anonFile.ErrorCode);
                    Console.WriteLine("Error Message: {0}", anonFile.ErrorMessage);
                    Console.WriteLine("Error Type: {0}", anonFile.ErrorType);
                    Console.WriteLine("Full URL: {0}", anonFile.FullUrl);
                    Console.WriteLine("Status: {0}", anonFile.Status);
                    Console.WriteLine("Download URL: {0}", anonFileWrapper.GetDirectDownloadLinkFromLink(anonFile.FullUrl));
                }
            }

            Console.ReadKey();
        }
    }
}